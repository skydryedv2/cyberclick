import java.io.*;

public class Problema2_parte2 {
    public static void main(String[] args) {
        try {
            //Reading File
            FileInputStream fstream = new FileInputStream("Resources/input.txt");
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));

            //Counter of valid passwords
            int valids = 0;

            //Reading line per line
            String strLine;
            while ((strLine = br.readLine()) != null) {
                //Spliting each line per space " "
                String[] tokens = strLine.split(" ");
                //Saving Each Column in Class Object data
                Data data = new Data(tokens[0],tokens[1],tokens[2]);

                //First Column splitting by -
                String[] firstField = data.firstValue.split("-");
                //Extracting Minimum Value
                int minValue = Integer.parseInt(firstField[0]);
                //Extracting Maximum Value
                int maxValue = Integer.parseInt(firstField[1]);

                //Second Column splitting by :
                String[] secondField = data.secondValue.split(":");
                //Getting the letter char
                String caracter = secondField[0];

                //looking each char position and comparing with min or Max Value (if it match add +1 to counter)
                for (int j=0; j<data.thirdValue.length(); j++){
                    if ((minValue-1) == j || (maxValue-1) == j){
                        if (caracter.equalsIgnoreCase(String.valueOf(data.thirdValue.charAt(j)))){
                            //if found at min or max position add +1 to counter and break the loop
                            valids++;

                            /*System.out.println(minValue-1 + " MinValue "+data.thirdValue.charAt(minValue-1));
                            System.out.println(maxValue-1 + " MaxValue "+data.thirdValue.charAt(maxValue-1));
                            System.out.println(j+ " Value of J "+data.thirdValue.charAt(j));*/
                            break;
                        }
                    }
                }
            }
            in.close();
            //Resultado es 693
            System.out.println("Contraseñas validas: " + valids);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
