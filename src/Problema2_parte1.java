import java.io.*;
import java.util.ArrayList;

public class Problema2_parte1 {
    public static void main(String[] args) {
        try {
            //Reading File
            FileInputStream fstream = new FileInputStream("Resources/input.txt");
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));

            //Counter of valid passwords
            int valids = 0;

            //Reading line per line
            String strLine;
            while ((strLine = br.readLine()) != null) {
                //Spliting each line per space " "
                String[] tokens = strLine.split(" ");
                //Saving Each Column in Class Object Data(String, String, String)
                Data data = new Data(tokens[0],tokens[1],tokens[2]);

                //First Column
                String[] firstField = data.firstValue.split("-");
                //Extracting Minimum Value
                int minValue = Integer.parseInt(firstField[0]);
                //Extracting Maximum Value
                int maxValue = Integer.parseInt(firstField[1]);

                //Second Column
                String[] secondField = data.secondValue.split(":");
                //Getting the char letter
                String caracter = secondField[0];

                //counting chars that are equals
                int counter = 0;

                //looking char by char and comparing if it is equal (if it is add +1 to counter)
                for (int j=0; j<data.thirdValue.length(); j++){
                    if (caracter.equalsIgnoreCase(String.valueOf(data.thirdValue.charAt(j)))){
                        counter ++;
                    }
                }
                //System.out.println("counter: " + counter + " Min Value: "+ minValue+ " Max Value: " + maxValue);

                //looking if the password it is valid or not
                if (counter >= minValue && counter <= maxValue){
                    valids ++;
                }
            }
            in.close();
            //Resultado es 643 Validas
            System.out.println("Contraseñas validas: " + valids);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}